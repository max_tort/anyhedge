const webpack = require('webpack')

module.exports = {
  context: __dirname,
  entry: './src/custodial.js',
  output: {
    path: __dirname + '/public',
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', { targets: "defaults", useBuiltIns: 'usage', corejs: { version: 3, proposals: true }}]
            ],
            plugins: ['@babel/plugin-proposal-class-properties']
          }
        }
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      process: 'process/browser',
    })
  ],
  resolve: {
    fallback: {
      'path': false,
      'os': require.resolve('os-browserify/browser'),
      'fs': false,
      'net': false,
      'tls': false,
      'util': require.resolve('util'),
      'assert': false,
      'process': false
    }
  }
}