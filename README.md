# AnyHedge contract example

Simple setup for AnyHedgeManager.

How to use:

1. `yarn` or `npm i` for installing the necessary packages
2. Our main file is located in `src/custodial.js`. You need to edit several parameters:

    1. `HEDGE_WIF` - stands for WIF of hedge party. Contains addresses, public and private keys. Note: you should have sufficient fund in order to fund the contract.
    2. `LONG_WIF` - stands for WIF of the long party. Contains addresses, public and private keys. Note: you should have sufficient fund in order to fund the contract.
    3. `AUTHENTICATION_TOKEN` provided by AnyHedge API.
        * For Linux: `curl -d 'name=<Your Name Here>' "https://api.anyhedge.com:6572/token"`
        * For Windows: `curl "https://api.anyhedge.com:6572/token" -d "name=<Your Name Here>"`

        You have to run the command in the terminal and paste the result like this: `const AUTHENTICATION_TOKEN = 'your_token'`
    4. `ORACLE_PUBLIC_KEY` - public key of Oracle provided by GeneralProtocols. Here is the [link](https://read.cash/@GeneralProtocols/general-protocols-launches-production-oracle-a8b62709)
    5. `NOMINAL_UNITS` - Set how many US cents that Hedge would like to protect against price volatility.
3. After setting the constants you can build. `yarn build` or `npm run build` for building the bundle. the bundle will be located here: `public/bundle.js`
4. open `index.html` in the chrome browser and open devtools. Open console and check whether the contract creation was successful.
